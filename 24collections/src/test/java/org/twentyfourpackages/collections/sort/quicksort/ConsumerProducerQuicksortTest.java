package org.twentyfourpackages.collections.sort.quicksort;

import org.twentyfourpackages.collections.sort.AbstractSorterTest;
import org.twentyfourpackages.collections.sort.Sorter;
import org.twentyfourpackages.collections.sort.quick.ConsumerProducerQuicksort;

public class ConsumerProducerQuicksortTest extends AbstractSorterTest {

  @Override
  protected Sorter getSorterToTest() {
    return new ConsumerProducerQuicksort();
  }

}
