package org.twentyfourpackages.collections.sort;

import java.security.InvalidParameterException;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractSorterTest {

  /**
   * Robustness test to check if an 0 element array doesn't break the code.
   */
  @Test
  public void test0() {
    int[] array = new int[0];
    this.getSorterToTest().sort(array);
  }

  /**
   * Simplest test.
   */
  @Test
  public void test1() {
    int[] array = new int[1];
    this.getSorterToTest().sort(array);
    Assert.assertEquals(0, array[0]);
  }

  /**
   * Basic test with 2 elements.
   */
  @Test
  public void test2() {
    int[] array = new int[2];
    array[0] = 1;
    array[1] = 0;
    this.getSorterToTest().sort(array);
    Assert.assertEquals(0, array[0]);
    Assert.assertEquals(1, array[1]);
  }


  /**
   * Slightly bigger test.
   */
  @Test
  public void testBigger() {
    int[] array = new int[5];
    array[0] = 4;
    array[1] = 3;
    array[2] = 2;
    array[3] = 1;
    array[4] = 0;
    this.getSorterToTest().sort(array);
    Assert.assertEquals(0, array[0]);
    Assert.assertEquals(1, array[1]);
    Assert.assertEquals(2, array[2]);
    Assert.assertEquals(3, array[3]);
    Assert.assertEquals(4, array[4]);
  }

  /**
   * Test for invalid argument.
   */
  @Test
  public void testInvalidArgument() {
    try {
      this.getSorterToTest().sort(new Object());
      Assert.fail();
    } catch (Exception e) {
      Assert.assertEquals(InvalidParameterException.class, e.getClass());
    }
  }


  /**
   * Big test. Sorts a big random array and checks if elements are in order in
   * the end.
   */
  @Test
  public void bigTest() {
    int[] toSort = this.generateRandomNumbers();
    this.getSorterToTest().sort(toSort);
    // toSort = this.generateRandomNumbers();
    // this.getSorterToTest().sort(toSort);
    // toSort = this.generateRandomNumbers();
    // this.getSorterToTest().sort(toSort);
    long start = System.currentTimeMillis();
    toSort = this.generateRandomNumbers();
    this.getSorterToTest().sort(toSort);
    System.out.println(System.currentTimeMillis() - start);
  }


  private static int[] randomNumbers;

  private int[] generateRandomNumbers() {
    int size = 10000000;
    if (AbstractSorterTest.randomNumbers == null) {
      AbstractSorterTest.randomNumbers = new int[size];
      Random r = new Random();
      for (int i = 0; i < size; i++) {
        AbstractSorterTest.randomNumbers[i] = r.nextInt();
      }
    }
    int[] newNumbers = new int[size];
    System.arraycopy(AbstractSorterTest.randomNumbers, 0, newNumbers, 0, size);
    return newNumbers;
  }

  /**
   * Test classes should implement this method so the tests are run.
   * 
   * @return
   */
  protected abstract Sorter getSorterToTest();

}
