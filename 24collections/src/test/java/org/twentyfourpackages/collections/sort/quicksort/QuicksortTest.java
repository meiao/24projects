package org.twentyfourpackages.collections.sort.quicksort;

import org.twentyfourpackages.collections.sort.AbstractSorterTest;
import org.twentyfourpackages.collections.sort.Sorter;
import org.twentyfourpackages.collections.sort.quick.Quicksort;

public class QuicksortTest extends AbstractSorterTest {

  @Override
  protected Sorter getSorterToTest() {
    return new Quicksort();
  }

}
