package org.twentyfourpackages.collections.sort.insertion;

import org.twentyfourpackages.collections.sort.AbstractSorterTest;
import org.twentyfourpackages.collections.sort.Sorter;

public class InsertionSortTest extends AbstractSorterTest {

  @Override
  protected Sorter getSorterToTest() {
    return new InsertionSort();
  }

}
