package org.twentyfourpackages.collections.sort.bubble;

import org.twentyfourpackages.collections.sort.AbstractSorterTest;
import org.twentyfourpackages.collections.sort.Sorter;

/**
 * Tests for {@link Bubblesort}
 * 
 * @author meiao
 */
public class BubblesortTest extends AbstractSorterTest {

  @Override
  protected Sorter getSorterToTest() {
    return new Bubblesort();
  }
}
