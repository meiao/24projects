package org.twentyfourpackages.collections.sort.parcel;

import java.security.InvalidParameterException;

import org.twentyfourpackages.collections.sort.Parcel;

public abstract class AbstractParcel implements Parcel {


  protected final int l;
  protected final int r;

  public AbstractParcel(int l, int r) {
    this.l = l;
    this.r = r;
  }

  @Override
  public int getL() {
    return this.l;
  }

  @Override
  public int getR() {
    return this.r;
  }

  @Override
  public void compExch(int i, int j) {
    if (this.comp(i, j) > 0) {
      this.exch(i, j);
    }
  }

  /**
   * Verifies if the given {@link Object} is an instance of the given class and
   * returns the sabe {@link Object}, casted to the given class.
   * 
   * @param obj the {@link Object} to be casted
   * @param clazz the class to cast the <em>obj</em>
   * @return the <em>obj</em> casted to <em>clazz</em>
   * @throws InvalidParameterException if <em>obj</em> is not an instance of
   *         <em>clazz</em>
   */
  protected <T> T castTo(Object obj, Class<T> clazz) {
    if (!clazz.isInstance(obj)) {
      throw new InvalidParameterException("Expecting " + clazz.getSimpleName()
          + " but received " + obj.getClass().getSimpleName());
    }
    return clazz.cast(obj);
  }

}
