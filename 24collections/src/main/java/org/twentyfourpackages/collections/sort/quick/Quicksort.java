package org.twentyfourpackages.collections.sort.quick;

import org.twentyfourpackages.collections.sort.AbstractSorter;
import org.twentyfourpackages.collections.sort.Parcel;
import org.twentyfourpackages.collections.sort.Sorter;
import org.twentyfourpackages.collections.sort.insertion.InsertionSort;

/**
 * Simplest implementation of Quicksort. Uses right most element as pivot.
 * 
 * @author meiao
 * 
 */
public class Quicksort extends AbstractSorter {

  public static final int THRESHOLD = 50;

  public Sorter smallSorter;

  public Quicksort() {
    this.smallSorter = new InsertionSort();
  }

  @Override
  public void sort(Parcel parcel) {
    int l = parcel.getL();
    int r = parcel.getR();
    if (l >= r) {
      return;
    }

    if (r - l < ConsumerProducerQuicksort.THRESHOLD) {
      this.smallSorter.sort(parcel);
      return;
    }


    int storeIndex = l;
    while (l < r) {
      if (parcel.comp(l, r) < 0) {
        parcel.exch(l, storeIndex++);
      }
      l++;
    }
    parcel.exch(storeIndex, r);
    this.sort(parcel.subParcel(parcel.getL(), storeIndex - 1));
    this.sort(parcel.subParcel(storeIndex + 1, parcel.getR()));
  }
}
