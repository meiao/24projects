/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort;

import java.util.Comparator;

/**
 * Utility class, contains mostly comparators and exchangers.
 * 
 * @author meiao
 * 
 */
class SortUtils {

  /**
   * Compares elements array[i] and array[j] using the comparator. If the first
   * is bigger than the second, then exchange them.
   * 
   * @param array the array containing the elements
   * @param comparator the comparator to compare the elements
   * @param i index of the first element to be compared in the array
   * @param j index of the second element to be compared in the array
   * @return true if there was an exchange, false otherwise
   */
  static <T> boolean compExch(T[] array, Comparator<T> comparator, int i, int j) {
    if (comparator.compare(array[i], array[j]) > 0) {
      SortUtils.exch(array, i, j);
      return true;
    }
    return false;
  }

  /**
   * Exchanges elements with indexes i and j in a array.
   * 
   * @param array the array containing the elements
   * @param i index of the first element to be exchanged
   * @param j index of the second element to be exchanged
   */
  static void exch(Object[] array, int i, int j) {
    Object o = array[i];
    array[i] = array[j];
    array[j] = o;
  }

  /**
   * Compares elements array[i] and array[j] and informs which is bigger
   * 
   * @param array the array containing the elements
   * @param comparator the comparator to compare the elements
   * @param i index of the first element to be compared
   * @param j index of the second element to be compared
   * @return true if the first element is bigger than the second, false
   *         otherwise
   */
  static <T> boolean gt(T[] array, Comparator<T> comparator, int i, int j) {
    if (comparator.compare(array[i], array[j]) > 0) {
      return true;
    }
    return false;
  }

  /**
   * Compares elements t1 and t2 and informs which is bigger
   * 
   * @param comparator the comparator to compare the elements
   * @param t1 the first element to be compared
   * @param t2 the second element to be compared
   * @return true if the first element is bigger than the second, false
   *         otherwise
   */
  static <T> boolean gt(Comparator<T> comparator, T t1, T t2) {
    if (comparator.compare(t1, t2) > 0) {
      return true;
    }
    return false;
  }


  /**
   * Compares elements array[i] and array[j] and informs which is smaller
   * 
   * @param array the array containing the elements
   * @param comparator the comparator to compare the elements
   * @param i index of the first element to be compared
   * @param j index of the second element to be compared
   * @return true if the first element is smaller than the second, false
   *         otherwise
   */
  static <T> boolean lt(T[] array, Comparator<T> comparator, int i, int j) {
    if (comparator.compare(array[i], array[j]) < 0) {
      return true;
    }
    return false;
  }

  /**
   * Compares elements t1 and t2 and informs which is smaller
   * 
   * @param comparator the comparator to compare the elements
   * @param t1 the first element to be compared
   * @param t2 the second element to be compared
   * @return true if the first element is smaller than the second, false
   *         otherwise
   */
  static <T> boolean lt(Comparator<T> comparator, T t1, T t2) {
    if (comparator.compare(t1, t2) < 0) {
      return true;
    }
    return false;
  }

  /**
   * Creates a new array with the specified size. This method exists because you
   * can't instantiate T[]. And to suppress the unchecked warning.
   * 
   * @param size the size of the array to be created
   * @return the specified array
   */
  @SuppressWarnings("unchecked")
  static <T> T[] newArray(int size) {
    return (T[]) new Object[size];
  }

  /**
   * Checks if an array is sorted, according to the comparator.
   * 
   * @param array the array to check
   * @param comparator the comparator to use
   * @return true if the array is sorted, false otherwise
   */
  static <T> boolean check(T[] array, Comparator<T> comparator) {
    for (int i = 1; i < array.length; i++) {
      if (SortUtils.gt(comparator, array[i - 1], array[i])) {
        return false;
      }
    }
    return true;
  }

}
