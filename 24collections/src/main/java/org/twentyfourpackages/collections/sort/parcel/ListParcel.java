package org.twentyfourpackages.collections.sort.parcel;

import java.util.Comparator;
import java.util.List;

import org.twentyfourpackages.collections.sort.Parcel;

public class ListParcel extends AbstractParcel {

  private final List<Object> list;
  private final Comparator<Object> comparator;

  /**
   * Constructor for sorting the whole {@link List}.
   * 
   * @param list the list to be sorted
   */
  public ListParcel(List<Object> list, Comparator<Object> comparator) {
    super(0, list.size() - 1);
    this.list = list;
    this.comparator = comparator;
  }

  /**
   * Constructor for sorting part of an array.
   * 
   * @param list the list to be sorted
   * @param l the first index to be sorted
   * @param r the last index to be sorted
   * @throws IndexOutOfBoundsException if l or r are negative, or higher than
   *         (or equals) the size of the array.
   */
  public ListParcel(List<Object> list, Comparator<Object> comparator, int l,
      int r) {
    super(l, r);
    this.list = list;
    this.comparator = comparator;
  }


  @Override
  public int comp(int i, int j) {
    return this.comparator.compare(this.list.get(i), this.list.get(j));
  }

  @Override
  public void exch(int i, int j) {
    Object t = this.list.get(i);
    this.list.set(i, this.list.get(j));
    this.list.set(j, t);
  }

  @Override
  public Object getObject(int index) {
    return this.list.get(index);
  }

  @Override
  public void putObject(Object object, int index) {
    this.list.set(index, object);
  }

  @Override
  public Parcel subParcel(int l, int r) {
    return new ListParcel(this.list, this.comparator, l, r);
  }

}
