package org.twentyfourpackages.collections.sort;

import java.util.Comparator;

import org.twentyfourpackages.collections.sort.parcel.ParcelFactory;

public abstract class AbstractSorter implements Sorter {


  @Override
  public void sort(Object array) {
    Parcel parcel = ParcelFactory.create(array);
    this.sort(parcel);
  }

  @Override
  public void sort(Object array, Comparator<?> comparator) {
    Parcel parcel = ParcelFactory.create(array, comparator);
    this.sort(parcel);
  }

}
