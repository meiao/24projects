/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort;

import java.security.InvalidParameterException;
import java.util.Comparator;

/**
 * Interface for sorters. It would be clearer if all sorting algorithms used
 * only static methods. But this interface was created for testing purposes.
 * 
 * @author onuki
 */
public interface Sorter {

  /**
   * Sorts the array.
   * 
   * @param object the array to be sorted
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements.
   * 
   */
  void sort(Object object);

  /**
   * Sorts the parcel of work, using the given comparator.
   * 
   * @param object the array to be sorted
   * @param comparator the comparator to use. Will only be used if object is an
   *        array of {@link Object}
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements and the
   *         comparator given is null.
   */
  void sort(Object object, Comparator<?> comparator);

  /**
   * Sorts the given {@link Parcel}.
   * 
   * @param parcel the {@link Parcel} to be sorted
   */
  void sort(Parcel parcel);
}
