/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort;

import java.security.InvalidParameterException;
import java.util.Comparator;


/**
 * A unit of work to be sorted. All methods for sorting the array should be
 * here, so the sorter does not have to worry about the type of the elements or
 * the comparator to be used.
 * 
 * @author meiao
 * 
 */
public interface Parcel {

  /**
   * Compares the objects with indexes i and j and exchanges them if the element
   * in i is bigger than the element in j.
   * 
   * @param i the first index
   * @param j the second index
   */
  void compExch(int i, int j);

  /**
   * Compares the objects with indexes i and j.
   * 
   * @see Comparator#compare(Object, Object)
   * 
   * @param i the first index
   * @param j the second index
   */
  int comp(int i, int j);

  /**
   * Exchanges the objects with indexes i and j.
   * 
   * @param i the first index
   * @param j the second index
   */
  void exch(int i, int j);

  /**
   * @return the first index of the array that should be sorted.
   */
  int getL();

  /**
   * @return the last index of the array that should be sorted.
   */
  int getR();

  /**
   * Returns the {@link Object} at the given index. May box a primitive.
   * 
   * @param index the index of the element to be returned
   * @return the object at the given index
   */
  Object getObject(int index);

  /**
   * Insert an object at the given index. May receive a boxed primitive as the
   * object.
   * 
   * @param object the object to be put in the array
   * @param index the index to put the object
   * @throws InvalidParameterException if object is of the wrong type
   */
  void putObject(Object object, int index);

  /**
   * Returns a subparcel of this parcel. The subparcel should have the same
   * array (or List) and the same comparator (if one is present) and new values
   * for l and r.
   * 
   * @param l the new value for l
   * @param r the new value for r
   * @return a {@link Parcel} of the same type but with a new range
   */
  Parcel subParcel(int l, int r);
}
