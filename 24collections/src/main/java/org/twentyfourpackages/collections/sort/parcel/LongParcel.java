/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort.parcel;

import org.twentyfourpackages.collections.sort.Parcel;



/**
 * Parcel that contains an array of int.
 * 
 * @author meiao
 * 
 */
public class LongParcel extends AbstractParcel {

  private final long[] array;

  /**
   * Constructor for sorting the whole array.
   * 
   * @param array
   */
  public LongParcel(long[] array) {
    super(0, array.length - 1);
    this.array = array;
  }

  /**
   * Constructor for sorting part of an array.
   * 
   * @param array the array to be sorted
   * @param l the first index to be sorted
   * @param r the last index to be sorted
   * @throws IndexOutOfBoundsException if l or r are negative, or higher than
   *         (or equals) the size of the array.
   */
  public LongParcel(long[] array, int l, int r) {
    super(l, r);
    this.array = array;
  }

  @Override
  public void exch(int i, int j) {
    long t = this.array[i];
    this.array[i] = this.array[j];
    this.array[j] = t;
  }

  @Override
  public int comp(int i, int j) {
    return (this.array[i] < this.array[j] ? -1
        : (this.array[i] == this.array[j] ? 0 : 1));
  }

  @Override
  public Object getObject(int index) {
    return Long.valueOf(this.array[index]);
  }

  @Override
  public void putObject(Object object, int index) {
    Long value = this.castTo(object, Long.class);
    this.array[index] = value.longValue();
  }

  @Override
  public Parcel subParcel(int l, int r) {
    return new LongParcel(this.array, l, r);
  }

}
