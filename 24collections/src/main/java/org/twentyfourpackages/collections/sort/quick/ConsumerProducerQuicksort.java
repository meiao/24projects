/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort.quick;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.twentyfourpackages.collections.sort.AbstractSorter;
import org.twentyfourpackages.collections.sort.Parcel;
import org.twentyfourpackages.collections.sort.Sorter;
import org.twentyfourpackages.collections.sort.insertion.InsertionSort;

/**
 * Parallel implementation of Quicksort. Uses the consumer/producer pattern to
 * parallelize the workload.
 * 
 * @author meiao
 * 
 */
public class ConsumerProducerQuicksort extends AbstractSorter {

  public static final int N_THREADS = 4;
  public static final int THRESHOLD = 500;

  public final BlockingQueue<Parcel> todo;

  public AtomicInteger left;

  public Sorter smallSorter;

  public ConsumerProducerQuicksort() {
    this.todo = new LinkedBlockingQueue<Parcel>();
    this.smallSorter = new InsertionSort();
  }

  @Override
  public synchronized void sort(Parcel parcel) {
    this.todo.clear();
    this.left = new AtomicInteger(0);

    this.add(parcel);

    Thread runners[] = new Thread[ConsumerProducerQuicksort.N_THREADS];

    for (int i = 0; i < ConsumerProducerQuicksort.N_THREADS; i++) {
      runners[i] = new Thread(new Worker());
      runners[i].start();
    }

    for (int i = 0; i < ConsumerProducerQuicksort.N_THREADS; i++) {
      synchronized (runners[i]) {
        while (runners[i].isAlive()) {
          try {
            runners[i].wait(100L);
          } catch (InterruptedException e) {
            runners[i].interrupt();
          }
        }
      }
    }

  }

  /**
   * Helper method to add a parcel both on the todo queue and the created queue.
   * 
   * @param p
   */
  private void add(Parcel p) {
    this.left.incrementAndGet();
    this.todo.add(p);
  }

  /**
   * Does one Quicksort pass on the given parcel, adds the 2 new parcels to be
   * done.
   * 
   * @param parcel
   */
  private void pass(Parcel parcel) {
    int l = parcel.getL();
    int r = parcel.getR();
    if (l >= r) {
      this.left.decrementAndGet();
      return;
    }

    if (r - l < ConsumerProducerQuicksort.THRESHOLD) {
      this.smallSorter.sort(parcel);
      this.left.decrementAndGet();
      return;
    }

    int storeIndex = l;
    while (l < r) {
      if (parcel.comp(l, r) < 0) {
        parcel.exch(l, storeIndex++);
      }
      l++;
    }
    parcel.exch(storeIndex, r);
    this.left.decrementAndGet();
    this.add(parcel.subParcel(parcel.getL(), storeIndex - 1));
    this.add(parcel.subParcel(storeIndex + 1, parcel.getR()));
  }

  /**
   * Checks if all work parcels are done.
   * 
   * @return true if there's no more work to be done, false otherwise
   */
  private boolean done() {
    if (!this.todo.isEmpty()) {
      return false;
    }
    if (this.left.intValue() != 0) {
      return false;
    }
    return true;
  }

  private class Worker implements Runnable {

    @Override
    public void run() {
      BlockingQueue<Parcel> queue = ConsumerProducerQuicksort.this.todo;
      Parcel parcel;

      while (true) {
        if (ConsumerProducerQuicksort.this.done()) {
          synchronized (this) {
            this.notify();
          }
          return;
        }
        try {
          parcel = queue.poll(100L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
          return;
        }
        if (parcel != null) {
          ConsumerProducerQuicksort.this.pass(parcel);
        }
      }
    }
  }

}
