package org.twentyfourpackages.collections.sort.parcel;

import java.lang.reflect.Method;
import java.util.List;

import org.twentyfourpackages.collections.sort.Parcel;

public class ComparableListParcel extends AbstractParcel {

  private final List<Comparable<?>> list;
  private static final Method compareTo;
  static {
    try {
      compareTo = Comparable.class.getMethod("compareTo", Object.class);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public ComparableListParcel(List<Comparable<?>> list) {
    super(0, list.size() - 1);
    this.list = list;
  }

  public ComparableListParcel(List<Comparable<?>> list, int l, int r) {
    super(l, r);
    this.list = list;
  }


  @Override
  public int comp(int i, int j) {
    try {
      return (Integer) ComparableListParcel.compareTo.invoke(this.list.get(i),
          this.list.get(j));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void exch(int i, int j) {
    Comparable<?> t = this.list.get(i);
    this.list.set(i, this.list.get(j));
    this.list.set(j, t);
  }

  @Override
  public Object getObject(int index) {
    return this.list.get(index);
  }

  @Override
  public void putObject(Object object, int index) {
    this.list.set(index, this.castTo(object, Comparable.class));
  }

  @Override
  public Parcel subParcel(int l, int r) {
    return new ComparableListParcel(this.list, l, r);
  }

}
