/*
 * This file is part of 24Projects.
 * 
 * 24Projects is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * 24Projects is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24Projects. If not, see <http://www.gnu.org/licenses/>.
 */
package org.twentyfourpackages.collections.sort.parcel;

import java.lang.reflect.Method;
import java.util.Comparator;

import org.twentyfourpackages.collections.sort.Parcel;



/**
 * Parcel that contains an array of int.
 * 
 * @author meiao
 * 
 */
public class ObjectParcel extends AbstractParcel {

  private final Object[] array;
  private final Comparator<?> comparator;
  private static final Method compare;

  static {
    try {
      compare =
          Comparator.class.getMethod("compare", Object.class, Object.class);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Constructor for sorting the whole array.
   * 
   * @param comparator the comparator to use
   * 
   * @param array
   */
  public ObjectParcel(Object[] array, Comparator<?> comparator) {
    super(0, array.length - 1);
    this.array = array;
    this.comparator = comparator;
  }

  /**
   * Constructor for sorting part of an array.
   * 
   * @param array the array to be sorted
   * @param comparator the comparator to use
   * @param l the first index to be sorted
   * @param r the last index to be sorted
   * @throws IndexOutOfBoundsException if l or r are negative, or higher than
   *         (or equals) the size of the array.
   */
  public ObjectParcel(Object[] array, Comparator<?> comparator, int l, int r) {
    super(l, r);
    this.array = array;
    this.comparator = comparator;
  }

  @Override
  public void exch(int i, int j) {
    Object t = this.array[i];
    this.array[i] = this.array[j];
    this.array[j] = t;
  }

  @Override
  public int comp(int i, int j) {
    try {
      return (Integer) ObjectParcel.compare.invoke(this.comparator,
          this.array[i], this.array[j]);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Object getObject(int index) {
    return this.array[index];
  }

  @Override
  public void putObject(Object object, int index) {
    this.array[index] = object;
  }

  public static void main(String... args) {
    System.out.println();
  }

  @Override
  public Parcel subParcel(int l, int r) {
    return new ObjectParcel(this.array, this.comparator, l, r);
  }
}
