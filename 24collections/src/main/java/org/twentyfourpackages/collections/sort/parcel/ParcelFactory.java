package org.twentyfourpackages.collections.sort.parcel;

import java.lang.reflect.Array;
import java.security.InvalidParameterException;
import java.util.Comparator;
import java.util.List;

import org.twentyfourpackages.collections.sort.Parcel;

/**
 * Methods for creating Parcels.
 * 
 * @author meiao
 * 
 */
public final class ParcelFactory {

  /**
   * Private constructor to prevent instantiation.
   */
  private ParcelFactory() {
  }


  /**
   * Creates a {@link Parcel} according to the given array. The resulting
   * {@link Parcel} should sort the whole array.
   * 
   * @param object the array to be sorted
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements.
   * @return one instance of a {@link Parcel}
   */
  public static Parcel create(Object object) {
    return ParcelFactory.create(object, null);
  }

  /**
   * Creates a {@link Parcel} according to the given array, using the given
   * comparator. The resulting {@link Parcel} should sort the whole array.
   * 
   * @param object the array to be sorted
   * @param comparator the comparator to use
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements.
   * @return one instance of a {@link Parcel}
   */
  public static Parcel create(Object object, Comparator<?> comparator) {
    if (!object.getClass().isArray()) {
      throw new InvalidParameterException("Object is not an array.");
    }
    int size = Array.getLength(object);
    return ParcelFactory.create(object, comparator, 0, size - 1);
  }

  /**
   * Creates a {@link Parcel} according to the given array.
   * 
   * @param object the array to be sorted
   * @param l the first index to sort
   * @param r the last index to sort
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements.
   * @return one instance of a {@link Parcel}
   */
  public static Parcel create(Object object, int l, int r) {
    if (!object.getClass().isArray()) {
      throw new InvalidParameterException("Object is not an array.");
    }
    return ParcelFactory.create(object, null, l, r);
  }

  /**
   * Creates a {@link Parcel} according to the given array.
   * 
   * @param object the array to be sorted
   * @param comparator the comparator to use. It will only be used if object is
   *        an array of {@link Object}.
   * @param l the first index to sort
   * @param r the last index to sort
   * @throws InvalidParameterException if the object given is not an array, or
   *         if it is an array of non-{@link Comparable} elements and the
   *         comparator given is null.
   * @return one instance of a {@link Parcel}
   */
  public static Parcel create(Object object, Comparator<?> comparator, int l,
      int r) {

    if (object instanceof List<?>) {
      return ParcelFactory.createList((List<?>) object, comparator, l, r);
    }
    if (!object.getClass().isArray()) {
      throw new InvalidParameterException("Object is not an array.");
    }

    Class<?> clazz = object.getClass().getComponentType();


    if (boolean.class.equals(clazz)) {
      return new BooleanParcel((boolean[]) object, l, r);
    }
    if (byte.class.equals(clazz)) {
      return new ByteParcel((byte[]) object, l, r);
    }
    if (char.class.equals(clazz)) {
      return new CharParcel((char[]) object, l, r);
    }
    if (double.class.equals(clazz)) {
      return new DoubleParcel((double[]) object, l, r);
    }
    if (float.class.equals(clazz)) {
      return new FloatParcel((float[]) object, l, r);
    }
    if (int.class.equals(clazz)) {
      return new IntParcel((int[]) object, l, r);
    }
    if (long.class.equals(clazz)) {
      return new LongParcel((long[]) object, l, r);
    }
    if (short.class.equals(clazz)) {
      return new ShortParcel((short[]) object, l, r);
    }

    if (comparator != null) {
      return new ObjectParcel((Object[]) object, comparator);
    }

    if (Comparable.class.isAssignableFrom(clazz)) {
      return new ComparableParcel((Object[]) object, l, r);
    }


    throw new InvalidParameterException("Unrecognized array type.");
  }


  private static Parcel createList(List<?> object, Comparator<?> comparator,
      int l, int r) {
    // TODO instantiate list parcels correctly
    throw new UnsupportedOperationException(
        "Sorting Lists is not available yet.");
  }

}
