package org.twentyfourpackages.collections.sort.insertion;

import org.twentyfourpackages.collections.sort.AbstractSorter;
import org.twentyfourpackages.collections.sort.Parcel;

public class InsertionSort extends AbstractSorter {

  @Override
  public void sort(Parcel parcel) {
    int l = parcel.getL();
    int r = parcel.getR();

    for (int i = l + 1; i <= r; i++) {
      for (int j = i; j > l; j--) {
        if (parcel.comp(j - 1, j) > 0) {
          parcel.exch(j - 1, j);
        } else {
          break;
        }
      }
    }
  }

}
