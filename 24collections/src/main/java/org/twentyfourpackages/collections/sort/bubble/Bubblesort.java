package org.twentyfourpackages.collections.sort.bubble;

import org.twentyfourpackages.collections.sort.AbstractSorter;
import org.twentyfourpackages.collections.sort.Parcel;

/**
 * Generic implementation of Bubblesort.
 * 
 * @author meiao
 * 
 */
public class Bubblesort extends AbstractSorter {

  @Override
  public void sort(Parcel parcel) {
    int l = parcel.getL();
    int r = parcel.getR();

    for (int i = r; i > l; i--) {
      for (int j = l; j < i; j++) {
        parcel.compExch(j, j + 1);
      }
    }
  }


}
